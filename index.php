<?php 
	require_once "logica/Producto.php";
	require_once "logica/Tienda.php";
	require_once "logica/Tienda_producto.php";

	
$pid = "";

if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);    
}
if(isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])){
    $_SESSION["id"]="";
}
?>


<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />	
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>	
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
</head>
<body>
	<?php 
	 	
	
	 
	    
	    $paginasSinSesion = array(
	        
			"presentacion/producto/crearProducto.php",
			"presentacion/producto/consultarProductoPagina.php",
			"presentacion/producto/consultarProductoTodos.php",
			"grafica.php",
	    );
	    
	    if($pid!=""){
	        include "presentacion/encabezado.php";
	        include "presentacion/menuPublico.php";
	        
	        include $pid;
	    }else{
	        include "presentacion/encabezado.php";
	        include "presentacion/menuPublico.php";	        
	        include "presentacion/inicio.php";
	        
	    }
	    
	
	?>	
	
	
	
	
</body>
</html>