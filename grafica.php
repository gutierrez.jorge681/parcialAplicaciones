<?php
$producto = new Producto();
$productos = $producto->consultarTodos();
$productosOrdenados = $producto->consultarTodosOrden();
$contOtros=0;
?>
<html>

<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

//-------------------------------------------------------------------------------------------------       
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChartt);

        function drawChartt() {

            var data = google.visualization.arrayToDataTable([
                ['Producto', 'Cantidad'],

                <?php
                $maximo=1;
                foreach ($productosOrdenados as $prodActual) {
                    if($maximo<10){
                        echo "['" . $prodActual->getNombre() . "', " . $prodActual->getPrecio() . "],";
                    }else{
                        $contOtros+= $prodActual->getPrecio();
                    }
                    $maximo++;
                }
                echo "['" . "otros" . "', " . $contOtros . "],";
                ?>

            ]);

            var options = {
                title: 'Productos'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechartt'));

            chart.draw(data, options);
        }

//-------------------------------------------------------------------------------------------------       
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Producto', 'Cantidad'],

                <?php
                foreach ($productos as $prodActual) {
                    if($prodActual->getPrecio()>20){
                        echo "['" . $prodActual->getNombre() . "', " . $prodActual->getPrecio() . "],";
                    }else{
                        $contOtros+= $prodActual->getPrecio();
                    }
                }
                echo "['" . "otros" . "', " . $contOtros . "],";
                ?>

            ]);

            var options = {
                title: 'Productos'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }
//-----------------------------------------------------------------------------------------------------
        google.charts.load('current', {
            packages: ['corechart', 'bar']
        });
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {

            var data = google.visualization.arrayToDataTable([
                ['Producyo', 'Cantidad', ],
                <?php
                foreach ($productos as $prodActual) {
                    echo "['" . $prodActual->getNombre() . "', " . $prodActual->getPrecio() . "],";
                }
                ?>
            ]);

            var options = {
                title: 'Cantidad de productos',
                chartArea: {
                    width: '50%',
                    height: '100%'
                },
                hAxis: {
                    title: 'Total productos',
                    minValue: 0
                },
                vAxis: {
                    title: 'Productos'
                }
            };

            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
//----------------------------------------------------------------------------------        
    </script>
</head>

<body style="height: 2000;">
    <div class="container mt-5" style="height: 500px;">

        <div id="piechartt" style="height: 500px;"></div>        

        <div id="piechart" style="height: 500px;"></div>        
        
        <div id="chart_div" style="padding-top:20px;"></div>

    </div>



</body>

</html>