<?php
require 'libreria/fpdf.php';

class PDF extends FPDF
{
	function Header()
	{

		$this->SetFont('Arial', 'B', 15);
		$this->Cell(30);
		$this->Cell(130, 10, 'Sistemas', 0, 1, 'C');
		$this->SetFont('Arial', '', 15);
		$this->Cell(190, 10, 'Reportes Proyecto', 0, 1, 'C');
		$this->SetFont('Arial', 'B', 15);

		$this->Cell(190, 10, 'Reportes', 0, 0, 'C');

		$this->Image('img/sinimagen.png', 10, 10, 30);

		$this->Ln(20);
	}

	function Footer()
	{
		$this->SetY(-50);
		$this->Cell(190, 10, 'Jorge Gutierrez', 0, 1,'C');
		$this->Cell(190, 10, 'joagutierrezp@correo.udistrital.edu.co', 0, 0,'C');
		$this->SetDrawColor(23, 32, 42);
		$this->SetLineWidth(1);
		$this->Line(10, 270, 195, 270);
		$this->SetY(-15);
		$this->SetFont('Arial', 'I', 8);
		$this->Cell(0, 10, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
	}
}
