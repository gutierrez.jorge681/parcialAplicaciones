<?php
require "fpdf/fpdf.php";
require_once "logica/Producto.php";

$producto = new Producto();
$productos = $producto -> consultarTodos();


$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("Courier", "B", 20);
$pdf -> AddPage();
$pdf ->SetXY(0, 0);
$pdf -> Cell(216, 20, "Tienda Virtual", 0, 2, "C");
$pdf -> Cell(216, 15, "Reporte Productos", 0, 2, "C");
$image1 = "im/sinimagen.png"; 


//$pdf->Cell( 40, 40, $pdf->Image($image1, $pdf->GetX(), $pdf->GetY(), 33.78), 0, 0, 'L', false );
$pdf -> SetFont("Courier", "B", 10);

$pdf->Ln();
$pdf->Cell(10,10,"id",1,0,'C');
$pdf->Cell(40,10,"nombre",1,0,'C');
$pdf->Cell(20,10,"cantidad",1,0,'C');
$pdf->Cell(60,10,"foto",1,0,'C');
$pdf->Ln();
    $i=1;
    foreach($productos as $productoActual)
    {    
        if($productoActual -> getImagen()!=""){
            $image1 = $productoActual->getImagen();
        } else{
            $image1 = "img/sinimagen.png"; 
        }   
        
        $pdf->Cell(10,40,$i,1,0,'C');
        $pdf->Cell(40,40,$productoActual->getNombre(),1,0,'C');        
        $pdf->Cell(20,40,$productoActual->getCantidad(),1,0,'C');
        //$pdf->Cell(60,8,$productoActual->getImagen(),1);
        $pdf->Cell( 60, 40, $pdf->Image($image1, $pdf->GetX()+10, $pdf->GetY()+5, 20,), 1, 0, 'C', false );
        //$pdf->Cell(60,40,$productoActual->getCantidad(),1,0,'C');
        $pdf->Ln();
        $i++;
    }





$pdf -> Output();




?>