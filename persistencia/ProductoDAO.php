<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;    
    private $precio;
       
    public function ProductoDAO($idProducto = "", $nombre = "",$precio = ""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> precio = $precio;        
    }

    public function consultar(){
        return "select id,nombre, precio
                from producto
                where id = '" . $this -> idProducto .  "'";
    }    
    
    public function insertar(){
        return "insert into producto (id, nombre, precio)
                values ('" . $this -> idProducto . "', '" . $this -> nombre . "', '" . $this -> precio . "')";
    }
    
    public function consultarTodos(){
        return "select id, nombre, precio
                from producto";
    }

    public function consultarTodosOrden(){
        return "select id, nombre, precio
                from producto
                ORDER BY precio DESC;";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select id, nombre, precio
                from producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(id)
                from producto";
    }
 
    public function editar(){
        return "update producto
                set nombre = '" . $this -> nombre . "', precio = '" . $this -> precio . "'
                where id = '" . $this -> idProducto .  "'";
    }
  
    public function consultarFiltro($filtro){
        return "select id, nombre, precio
                from producto
                where nombre like '%" . $filtro . "%' or precio like '" . $filtro . "%'";
    }
    
}

?>