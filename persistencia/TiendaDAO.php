<?php
class TiendaDAO{
    private $idTienda;
    private $nombre;    
    private $direccion;
       
    public function TiendaDAO($idTienda = "", $nombre = "",$direccion = ""){
        $this -> idTienda = $idTienda;
        $this -> nombre = $nombre;
        $this -> direccion = $direccion;        
    }

    public function consultar(){
        return "select id,nombre, direccion
                from tienda
                where id = '" . $this -> idTienda .  "'";
    }    
    
    public function insertar(){
        return "insert into tienda (id, nombre, direccion)
                values ('" . $this -> idTienda . "', '" . $this -> nombre . "', '" . $this -> direccion . "')";
    }
    
    public function consultarTodos(){
        return "select id, nombre, direccion
                from tienda";
    }

   
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select id, nombre, direccion
                from tienda
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(id)
                from tienda";
    }
 
    public function editar(){
        return "update tienda
                set nombre = '" . $this -> nombre . "', direccion = '" . $this -> direccion . "'
                where id = '" . $this -> idTienda .  "'";
    }
  
    public function consultarFiltro($filtro){
        return "select id, nombre, direccion
                from tienda
                where nombre like '%" . $filtro . "%' or direccion like '" . $filtro . "%'";
    }
    
}

?>