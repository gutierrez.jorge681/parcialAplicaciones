<?php
class Tienda_productoDAO{
    private $item;
    private $idTienda;    
    private $idProducto;
       
    public function Tienda_productoDAO($item = "", $idTienda = "",$idProducto = ""){
        $this -> item = $item;
        $this -> idTienda = $idTienda;
        $this -> idProducto = $idProducto;        
    }

    public function consultar(){
        return "select id,idTienda, idProducto
                from tienda_producto
                where item = '" . $this -> item .  "'";
    }    
    
    public function consultarIdTienda(){
        return "select id, idTienda, idProducto
                from tienda_producto
                where idProducto = '" . $this -> idProducto .  "'";
    } 

    public function insertar(){
        return "insert into tienda_producto (idTienda, idProducto)
                values ('" . $this -> idTienda . "', '" . $this -> idProducto . "')";
    }
    
    public function consultarTodos(){
        return "select item, idTienda, idProducto
                from tienda_producto";
    }

    public function consultarTodosTienda(){
        return "select item, idTienda, idProducto
                from tienda_producto
                where item = '" . $this -> item .  "'";
    }


    public function consultarCantidad(){
        return "select count(item)
                from tienda_producto";
    }
 
    public function editar(){
        return "update tienda_producto
                set idTienda = '" . $this -> idTienda . "', idProducto = '" . $this -> idProducto . "'
                where item = '" . $this -> item .  "'";
    }
  
   
    
}

?>