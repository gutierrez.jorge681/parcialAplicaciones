<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Tienda_productoDAO.php";
class Tienda_producto{
    
    private $item;
    private $idTienda;    
    private $idProducto;

    private $conexion;
    private $tienda_productoDAO;
    
    public function getItem(){
        return $this -> item;
    }
    
    public function getidTienda(){
        return $this -> idTienda;
    }
    
    
    public function getidProducto(){
        return $this -> idProducto;
    }
        
   
    
    public function Tienda_producto($item = "", $idTienda = "", $idProducto = ""){
        $this -> item = $item;
        $this -> idTienda = $idTienda;        
        $this -> idProducto = $idProducto;        
        $this -> conexion = new Conexion();
        $this -> tienda_productoDAO = new Tienda_productoDAO($this -> item, $this -> idTienda, $this -> idProducto);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tienda_productoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> item = $resultado[0];
        $this -> idTienda = $resultado[1];
        $this -> idProducto = $resultado[2];        
    }   

    public function consultarIdTienda(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tienda_productoDAO -> consultarTodos());
       
        while(($resultado = $this -> conexion -> extraer()) != null){
            $this -> item = $resultado[0];
        $this -> idTienda = $resultado[1];
        $this -> idProducto = $resultado[2];
        }
       
        $this -> conexion -> cerrar();        
        
    } 

    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> tienda_productoDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tienda_productoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }   

    public function consultarTodosTienda(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tienda_productoDAO -> consultarTodosTienda());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Tienda_producto($resultado[0], $resultado[1], $resultado[2]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();        
        return $productos;
    }   
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tienda_productoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> tienda_productoDAO -> editar());
        $this -> conexion -> cerrar();
    }
  
    
    
}

?>